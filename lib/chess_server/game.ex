defmodule ChessServer.Game do

  defstruct(
    white_player: nil,
    black_player: nil,
    game_id: nil,
    move_turn: nil,
    fen: nil,
    game_state: nil,
    move_count: 0
  )

  def new_game() do
    with {:ok, game_id} <-  :binbo.new_server(),
    _ <- :binbo.new_game(game_id),
    {:ok, fen} <- :binbo.get_fen(game_id),
    {:ok, move_turn} <- :binbo.side_to_move(game_id),
    {:ok, game_state} <- :binbo.game_status(game_id) do

       %ChessServer.Game{
        white_player: nil,
        black_player: nil,
        game_id: game_id,
        fen: fen,
        move_turn: move_turn,
        game_state: game_state
      }
    end
  end

  def make_move(game = %{game_state: {:checkmate, :white_wins}}, _) do
   { game, "Game over. White wins" }
  end

  def make_move(game = %{game_state: {:checkmate, :black_wins}}, _) do
   { game, "Game over. Black wins" }
  end

  def make_move(game = %{game_state: {:draw, _}}, _) do
   { game, "Game Drawn" }
  end

  def make_move(game, move) do
    :binbo.move(game.game_id, <<"#{move}">>)
    IO.inspect(game.game_state)

    IO.inspect(:binbo.game_status(game.game_id))

    case :binbo.game_status(game.game_id) do
      {:ok, :continue} ->
        %{game | game_state: :continue, move_count: game.move_count + 1}


      {:ok, result} ->
        IO.inspect("result")
        IO.inspect(result)
        %{game | game_state: result, move_count: game.move_count + 1}

    end

  end

end
