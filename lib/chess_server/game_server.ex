defmodule ChessServer.GameServer do
  use GenServer

  defstruct(
    white_player: nil,
    black_player: nil,
    game_id: nil,
    move_turn: nil,
    fen: nil,
    game_state: nil,
    move_count: 0
  )

  def start_link do
    GenServer.start_link(__MODULE__, [self()], name: __MODULE__)
  end

  def init([player_pid]) do
    {:ok, game_pid} = :binbo.new_server()
    {:ok, :continue} = :binbo.new_game(game_pid)
    {:ok, fen} = :binbo.get_fen(game_pid)
    {:ok, move_turn} = :binbo.side_to_move(game_pid)
    {:ok, game_state} = :binbo.game_status(game_pid)

    # :sys.get_state(game) to print state
    # state is hidden inside the process
    # this is not objects. cannot use ChessServer.GameServer.state type of notation
    state = %ChessServer.GameServer{
      white_player: player_pid,
      black_player: nil,
      game_id: game_pid,
      fen: fen,
      move_turn: move_turn,
      game_state: game_state
    }
    {:ok, state}
  end


  #######################################################
  #               CLIENT SIDE
  #######################################################



  def make_move(move) do
    GenServer.call(__MODULE__, {:make_move, move})
  end

  def join(game_pid) do
    GenServer.call(game_pid, :join)
  end

  #######################################################
  #               SERVER SIDE
  #######################################################

  def handle_call({:make_move, move}, _from, state) do
    :binbo.move(state.game_id, move)
    state = %{state | fen: :binbo.get_fen(state.game_id)}
    :binbo.print_board(state.game_id)
    send(state.white_player, {:new_move, self()})
    send(state.black_player, {:new_move, self()})
    {:reply, state, state}
  end

  def handle_call(:join, _, %ChessServer.GameServer{white_player: white_pid, black_player: black_pid} = state)
    when is_pid(white_pid) and is_pid(black_pid) do
    {:reply, {:error, :game_full}, state}
  end

  def handle_call(:join, {black_player, _}, state) do
    state = %{state | black_player: black_player}
    IO.inspect(state)
    {:reply, {:ok, :joined}, state}
  end

  def handle_info({:new_move, game_pid}, state) do
    :binbo.print_board(game_pid)
    {:noreply, state}
  end

end