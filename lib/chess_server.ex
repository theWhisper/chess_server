defmodule ChessServer do

  alias ChessServer.Game

  defdelegate new_game(), to: Game

  defdelegate make_move(game, move), to: Game

end
