defmodule ChessServerTest do
  use ExUnit.Case
  doctest ChessServer

  alias ChessServer.Game

  test "white wins" do
    game = Game.new_game() |> Map.put(:game_state, {:checkmate, :white_wins})
    assert { ^game, _} = Game.make_move(game, "e2e4")
  end

  test "black wins" do
    game = Game.new_game() |> Map.put(:game_state, {:checkmate, :black_wins})
    assert { ^game, _} = Game.make_move(game, "e2e4")
  end

  test "draw" do
    game = Game.new_game() |> Map.put(:game_state, {:draw, :stalemate})
    assert { ^game, _} = Game.make_move(game, "e2e4")
  end



end
